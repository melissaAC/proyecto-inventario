<?php
Class Connection {
    private static $connection;

    public static function openConnection(){
        include_once 'config.inc.php';
        self::$connection = mysqli_connect($server,$user,$password,$database)
        or die("Ha sucedido un error inexperado en la conexion de la base de datos");
    }

    public static function closeConnection(){
        $close = mysqli_close(self::$connection) 
        or die("Ha sucedido un error inexperado en la desconexion de la base de datos");   
        return $close;
    }

    public static function getConnection(){
        return self::$connection;
    }

}