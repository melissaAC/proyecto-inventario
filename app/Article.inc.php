<?php

class Article{
    private $id;
    private $name;
    private $amount;
    private $units;
    private $dateRegister;
    private $type;
    private $lot;
    private $invima;

    public function __construct($id, $name, $amount, $units, $dateRegister, $type, $lot, $invima){
        $this -> id = $id;
        $this -> name = $name;
        $this -> amount = $amount;
        $this -> units = $units;
        $this -> dateRegister = $dateRegister;
        $this -> type = $type;
        $this -> lot = $lot;
        $this -> invima = $invima;
    }

    public function getId(){
        return $this -> id;
    }

    public function getName(){
        return $this -> name;
    }

    public function getAmount(){
        return $this -> amount;
    }

    public function getUnits(){
        return $this -> units;
    }

    public function getDate(){
        return $this -> dateRegister;
    }

    public function getType(){
        return $this -> type;
    }

    public function getLot(){
        return $this -> lot;
    }

    public function getInvima(){
        return $this -> invima;
    }

    public function setId($id){
        return $this -> id = $id;
    }

    public function setName($name){
        return $this -> name = $name;
    }

    public function setAmount($amount){
        return $this -> amount = $amount;
    }

    public function setUnits($units){
        return $this -> units = $units;
    }

    public function setDate($dateRegister){
        return $this -> dateRegister = $dateRegister;
    }

    public function setType($type){
        return $this -> type = $type;
    }

    public function setLot($lot){
        return $this -> lot = $lot;
    }

    public function setInvima($invima){
        return $this -> invima = $invima;
    }
}