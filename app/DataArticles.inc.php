<?php
include_once 'Article.inc.php';
class DataArticles {

    //RETORNA TODOS LOS ELEMENTOS DE LA TABLA CREANDO UN ARREGLO DE ARTICLES
    public static function allArticles ($connection){
        $articles = array();
        if(isset($connection)){
            $sql = "SELECT * FROM articles";
            if(!$results = $connection -> query($sql)){
                print "Lo sentimos, este sitio web está experimentando problemas.";
                print "Error: La ejecución de la consulta falló debido a: \n";
                print "Query: " . $sql . "\n";
                print "Errno: " . $connection->errno . "\n";
                print "Error: " . $connection->error . "\n";
                exit;
            }
            if (count($results) != 0) {
                foreach($results as $fila){
                    $articles[] = new Article(
                        $fila['id'],
                        $fila['name'],
                        $fila['amount'],
                        $fila['units'],
                        $fila['dateRegister'],
                        $fila['type'],
                        $fila['lot'],
                        $fila['invima']
                    );
                }
            }else{
                print 'Sin resultados';
            }
        }
        return $articles;
    }

    //CONSULTA DEL NUMERO DE REGISTROS DE LA TABLA
    public static function countArticles ($connection){
        $articles = array();
        if(isset($connection)){
            //sentencia sql
            $sql = "SELECT COUNT(*) as total FROM articles";
            if(!$results = $connection -> query($sql)){
                print "Lo sentimos, este sitio web está experimentando problemas.";
                print "Error: La ejecución de la consulta falló debido a: \n";
                print "Query: " . $sql . "\n";
                print "Errno: " . $connection->errno . "\n";
                print "Error: " . $connection->error . "\n";
                exit;
            }
        }
        $total;
        if (count($results) != 0) {
            foreach($results as $fila){
                $total = $fila['total'];
            }
        }else{
            print 'Sin resultados';
        }
        return $total;
    }

    //ELIMINA EN LA BASE DE DATOS POR MEDIO DE ID
    public static function deleteArticle($connection, $id){
        if(isset($connection)){
            //sentencia sql
            $sql = "DELETE FROM articles WHERE id = $id";
            if(!$results = $connection -> query($sql)){
                print "Lo sentimos, este sitio web está experimentando problemas.";
                print "Error: La ejecución de la consulta falló debido a: \n";
                print "Query: " . $sql . "\n";
                print "Errno: " . $connection->errno . "\n";
                print "Error: " . $connection->error . "\n";
                exit;
            }
            print $results;
            return $results?true:false;
        }
    }

    //INSERTA EN LA BASE DE DATOS UN NUEVO REGISTRO
    public static function insertArticle($connection, $id, $name, $amount, $units, $dateRegister, $type, $lot, $invima){
        if(isset($connection)){
            //sentencia sql
            $sql = "INSERT INTO articles (id, name, amount, units, dateRegister, type, lot, invima) Values 
            ($id, $name, $amount, $units, $dateRegister, $type, $lot, $invima)";
            if(!$results = $connection -> query($sql)){
                print "Lo sentimos, este sitio web está experimentando problemas.";
                print "Error: La ejecución de la consulta falló debido a: \n";
                print "Query: " . $sql . "\n";
                print "Errno: " . $connection->errno . "\n";
                print "Error: " . $connection->error . "\n";
                exit;
            }
            print $results;
            return $results?true:false;
        }
    }

    //ACTUALIZA EN LA BASE DE DATOS POR  MEDIO DE ID
    public static function updateArticle($connection, $id, $name, $amount, $units, $dateRegister, $type, $lot, $invima){
        if(isset($connection)){
            //sentencia sql
            $sql = "UPDATE articles SET name=$name, amount=$amount, units=$units, dateRegister=$dateRegister, type=$type, lot=$lot, invama=$invima WHERE id=$id";
            if(!$results = $connection -> query($sql)){
                print "Lo sentimos, este sitio web está experimentando problemas.";
                print "Error: La ejecución de la consulta falló debido a: \n";
                print "Query: " . $sql . "\n";
                print "Errno: " . $connection->errno . "\n";
                print "Error: " . $connection->error . "\n";
                exit;
            }
            print $results;
            return $results?true:false;
        }
    }
}