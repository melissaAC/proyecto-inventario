<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Inventario Traigo</title>

    <!-- Bootstrap Style-->
    <!--<link rel="stylesheet" href="./css/bootstrap.min.css">-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

</head>
<body >
    <!-- si no se oculta el div con style="display:none;" del php salen los warnings en el body de la pagina-->
    <div style="display:none;">
        <?php
            include_once 'app/Connection.inc.php';
            include_once 'app/DataArticles.inc.php';
            include_once 'app/Article.inc.php';
            Connection :: openConnection();
            $allArticles = DataArticles :: allArticles(Connection :: getConnection());
            $countArticles = DataArticles :: countArticles(Connection :: getConnection());
        ?>
    </div>

    <div class="container">
        <!-- Button Modal -->
        <button type="button" onclick="return addNewArticle();" class="btn btn-primary" data-toggle="modal" data-target="#formArticles">
            Nuevo artículo
        </button>
        
        <!-- Modal -->
        <div class="modal fade" id="formArticles" tabindex="-1" role="dialog" aria-labelledby="formArticleLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form  onsubmit="return addOrUpdateArticle();">

                        <div class="modal-header">
                            <h5 class="modal-title" id="formAticlesLabel">Artículo</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>

                        <div class="modal-body">
                            <input type="hidden" class="form-control" id="flag">
                            
                            <div class="form-row">
                                <div class="col-6">
                                    <label for="name">Nombre</label>
                                    <input type="text" class="form-control" id="name">
                                </div>
                                <div class="col-3">
                                    <label for="amount">Cantidad</label>
                                    <input type="number" class="form-control" id="amount">
                                </div>
                                <div class="col-3">
                                    <label for="units">Unidades</label>
                                    <input type="text" class="form-control" id="units">
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-6">
                                    <label for="date">Fecha</label>
                                    <input type="date" class="form-control" id="date">
                                </div>
                                <div class="col-6">
                                    <label for="type">Tipo artículo</label>
                                    <input type="text" class="form-control" id="type">
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-6">
                                    <label for="lot">Lote</label>
                                    <input type="text" class="form-control" id="lot">
                                </div>
                                <div class="col-6">
                                    <label for="invima">INVIMA</label>
                                    <input type="text" class="form-control" id="invima">
                                </div>
                            </div>
                            
                        </div>
                        
                        <div class="modal-footer">
                            <div class="form-row">
                                <div class="col-6">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                </div>
                                <div class="col-6">
                                    <button type="submit" class="btn btn-primary">Guardar</button>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
    
    <div class="container">
        <table class="table" id="tableArticles">
            <thead>
              <tr>
                <th>#</th>
                <th>Nombre</th>
                <th>Cantidad</th>
                <th>Unidades</th>
                <th>Fecha</th>
                <th>Tipo</th>
                <th>Lote</th>
                <th>INVIMA</th>
                <th>Accion</th>
              </tr>
            </thead>
            <tbody>
                <?php
                    foreach($allArticles as $article){
                ?>
                    <tr>
                        <td><?php echo $article -> getId() ?></td>
                        <td><?php echo $article -> getName() ?></td>
                        <td><?php echo $article -> getAmount() ?></td>
                        <td><?php echo $article -> getUnits() ?></td>
                        <td><?php echo $article -> getDate() ?></td>
                        <td><?php echo $article -> getType() ?></td>
                        <td><?php echo $article -> getLot() ?></td>
                        <td><?php echo $article -> getInvima() ?></td>
                        <td>
                            <div class="form-row">

                                <div class="col-6">
                                    <input id="ver" type="button"  onclick="seeArticle(<?php echo $article -> getId() ?>);" class="btn btn-info" value="ver | editar">
                                </div>
                                <div class="col-6">
                                    <input id="eliminar" type="button" onclick="deleteArticle(<?php echo $article -> getId() ?>);" class="btn btn-danger" value="eliminar">
                                </div>

                            </div>
                        </td>
                    </tr>
                <?php   
                    }
                ?>
            </tbody>
          </table>
    </div>

    <!-- Bootstrap js-->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>

    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    
    <script src="js/logicaIndex.js"></script>
    <script>
        loadArticles();
    </script>

</body>
</html>