
//listado de articulos
let listArticle = [];

//lee los articulos del <tbody>
function loadArticles(){
    let table = document.getElementById('tableArticles');
    let tbody = table.children.item(1);
    for (let i = 0; i < tbody.childElementCount; i++) {
        article = tbody.children.item(i);
        let id = article.children.item(0).innerHTML;
        let name = article.children.item(1).innerHTML;
        let amount = article.children.item(2).innerHTML;
        let units = article.children.item(3).innerHTML;
        let date = article.children.item(4).innerHTML;
        let type = article.children.item(5).innerHTML;
        let lot = article.children.item(6).innerHTML;
        let invima = article.children.item(7).innerHTML;
        createArticle(id,name,amount, units, date, type, lot, invima);
    }
}

//agrega los articulos nuevos
function addArticle(){
    //obtiene los datos
    let name = document.getElementById('name').value;
    let amount = document.getElementById('amount').value;
    let units = document.getElementById('units').value;
    let date = document.getElementById('date').value;
    let type = document.getElementById('type').value;
    let lot = document.getElementById('lot').value;
    let invima = document.getElementById('invima').value;
    
    createArticle(-1,name,amount, units, date, type, lot, invima);
    
    //limpia el formulario
    clearForm();

    //actualiza la vista de los articulos
    viewArticle();

    //articulo agregado exitosamente
    successfuladdArticle();
}

//confirma que se agrego correctamente
function successfuladdArticle(){
    Swal.fire({
        icon: 'success',
        title: 'Exitoso',
        showConfirmButton: false,
        timer: 1500
      })

      $('#formArticles').modal('hide');
}

//crear articulo y agregarlo a la lista de articulos
function createArticle(id,name,amount, units, date, type, lot, invima){
    
    //crea el objeto article
    const article = {
        id:id,
        name:name,
        amount:amount,
        units:units,
        date:date,
        type:type,
        lot:lot,
        invima:invima
    }

    //guarda el nuevo articulo en la ista de articulos
    listArticle.push(article);
}

// si la bandera es "nuevo" agrega nuevo articulo,
// si la bandera no es "nuevo" actualiza articulo
function addOrUpdateArticle(){

        let flag = document.getElementById('flag').value;
        if(flag == 'nuevo'){
            addArticle();
        }else{
            updateArticle();
        }

        //$('#formArticles').modal('hide');

        return false;
}

//actualiza el articulo
function updateArticle(){
    let id = document.getElementById('flag').value;

    listArticle[id].name = document.getElementById('name').value;
    listArticle[id].amount = document.getElementById('amount').value;
    listArticle[id].units = document.getElementById('units').value;
    listArticle[id].date = document.getElementById('date').value;
    listArticle[id].type = document.getElementById('type').value;
    listArticle[id].lot = document.getElementById('lot').value;
    listArticle[id].invima = document.getElementById('invima').value;

    //limpia el formulario
    clearForm();

    //ver tabla articulos
    viewArticle();

    //articulo modificado exitosamente
    successfuladdArticle();

}

//idenficia que es un nuevo ingreso de articulos
function addNewArticle(){
    document.getElementById('flag').value = 'nuevo';
}

//visualiza un articulo
function seeArticle(i){
    document.getElementById('name').value = listArticle[i].name;
    document.getElementById('amount').value = listArticle[i].amount;
    document.getElementById('units').value = listArticle[i].units;
    document.getElementById('date').value = listArticle[i].date;
    document.getElementById('type').value = listArticle[i].type;
    document.getElementById('lot').value = listArticle[i].lot;
    document.getElementById('invima').value = listArticle[i].invima;
    document.getElementById('flag').value = listArticle[i].id;

    $('#formArticles').modal('show');

}

//elimina un articulo
function deleteArticle(i){

    //sweetalert utlilizado
    Swal.fire({
        title: '¿Esta seguro de eliminar el articulo?',
        text: "¡No podrias recuperar el registro!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, eliminar!'
    }).then((result) => {
        if (result.value) {

            //elimina el registro y actualiza
            listArticle.splice(i, 1);
            viewArticle();

            Swal.fire(
                'Eliminado!',
                'El registro a sido eliminado.',
                'success'
        )
        }
    })

}

//vista de los articulos
function viewArticle(){
    //limpia la tabla
    clearTable();

    // busca la tabla por id y crea <tbody>
    let table = document.getElementById('tableArticles');
    let tb = document.createElement('tbody');

    // por cada articulo se va creado la respectiva fila <tr>
    // indicando que va en cada celda con el <hr>
    for (let i = 0; i < listArticle.length; i++) {

        let tr = document.createElement('tr');

        //agrega el id del articulo a la tabla
        let tdId = document.createElement('td');
        tdId.appendChild(document.createTextNode(i));
        listArticle[i].id = i;
        tr.appendChild(tdId);

        //agrega el nombre del articulo a la tabla
        let tdName = document.createElement('td');
        tdName.appendChild(document.createTextNode(listArticle[i].name));
        tr.appendChild(tdName);

        //agrega la cantidad del articulo a la tabla
        let tdAmount = document.createElement('td');
        tdAmount.appendChild(document.createTextNode(listArticle[i].amount));
        tr.appendChild(tdAmount);

        //agrega la unidades del articulo a la tabla
        let tdUnits = document.createElement('td');
        tdUnits.appendChild(document.createTextNode(listArticle[i].units));
        tr.appendChild(tdUnits);

        //agrega la fecha del articulo a la tabla
        let tdDate = document.createElement('td');
        tdDate.appendChild(document.createTextNode(listArticle[i].date));
        tr.appendChild(tdDate);

        //agrega el tipo del articulo a la tabla
        let tdType = document.createElement('td');
        tdType.appendChild(document.createTextNode(listArticle[i].type));
        tr.appendChild(tdType);

        //agrega el lote del articulo a la tabla
        let tdLot = document.createElement('td');
        tdLot.appendChild(document.createTextNode(listArticle[i].lot));
        tr.appendChild(tdLot);

        //agrega el invima del articulo a la tabla
        let tdInvima = document.createElement('td');
        tdInvima.appendChild(document.createTextNode(listArticle[i].invima));
        tr.appendChild(tdInvima);

        //crea las acciones por articulo a la tabla
        let tbButtons = document.createElement('td');
        tbButtons.innerHTML += 
        "<div class='form-row'>"+
            "<div class='col-6'>"+
                "<button id='ver' type='button'  onclick='seeArticle("+i+")' class='btn btn-info'>ver | editar</button>"+
            "</div>"+
            "<div class='col-6'>"+
                "<button id='eliminar' type='button' onclick='deleteArticle("+i+")' class='btn btn-danger'>eliminar</button>"+
            "</div>"+
        "</div>";
        tr.appendChild(tbButtons);

        //agrega cada fila al <tbody>
        tb.appendChild(tr);
    }

    //el <tbody> se agrega a la tabla
    table.appendChild(tb);
}

//limpia la tabla de articulos
function clearTable(){

    //busca la tabla y reconstruye la tabla eliminando el <tbody>
    let elementsTable = document.getElementById('tableArticles');
    elementsTable.innerHTML = 
    "<thead>"+
        "<tr>"+
            "<th>#</th>"+
            "<th>Nombre</th>"+
            "<th>Cantidad</th>"+
            "<th>Unidades</th>"+
            "<th>Fecha</th>"+
            "<th>Tipo</th>"+
            "<th>Lote</th>"+
            "<th>INVIMA</th>"+
            "<th>Accion</th>"+
        "</tr>"+
    "</thead>";
}

//limpia los campos del formulario de ingreso de datos para el articulo
function clearForm(){

    document.getElementById('name').value = "";
    document.getElementById('amount').value = "";
    document.getElementById('units').value = "";
    document.getElementById('date').value = "";
    document.getElementById('type').value = "";
    document.getElementById('lot').value = "";
    document.getElementById('invima').value = "";
}
